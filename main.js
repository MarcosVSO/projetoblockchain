let hash = require('object-hash');
let BlockChain = require("./blockChain");
var readline = require('readline');

let blockChain = new BlockChain();

let PROVA = 100;
showMenu();
let verificaProva = (prova) => {
    let adivinhaHash = hash(prova);
    console.log("Hashing: ", adivinhaHash);
    return adivinhaHash == hash(PROVA);
};

let proofOfWork = () => {
    let prova = 0;
    while (true) {
        if (!verificaProva(prova)) {
            prova++;
        } else {
            break;
        }
    }
    return prova;
};

var readline = require('readline');
var log = console.log;

var rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

var remetente = "";
var destinatario = "";
var valor;

var recursiveAsyncReadLine = function () {
    rl.question('', function (answer) {
        if (answer == '/exit') {
            return rl.close();
        }
        else if (answer == '/transacao') {
            if (remetente != "" && destinatario != "" && valor != "") {
                if (proofOfWork() == PROVA) {
                    blockChain.adicionarNovaTransacao(remetente, destinatario, valor);
                    let hashAnterior = blockChain.ultimoBloco() ? blockChain.ultimoBloco().hash : null;
                    blockChain.adicionarBloco(hashAnterior);
                    console.log("Transação Efetuada com sucesso: ", remetente, "-", destinatario, "-", valor);
                }
            } else {
                console.log("Primeiro digite um nome de remetente, destinatário e o valor da transação");
            }
        }
        else if (answer == '/showBlock') {
            str = JSON.stringify(blockChain.chain, null, 4);
            console.log(str);
        }
        else if (answer.substring(0, 10) == '/remetente') {
            remetente = answer.substring(11);
            //console.log(remetente);
        }
        else if (answer.substring(0, 13) == '/destinatario') {
            destinatario = answer.substring(14);
            //console.log(destinatario);
        }
        else if (answer.substring(0, 4) == '/qtd') {
            valor = answer.substring(5);
            //console.log(valor);
        }
        else if (answer == '/menu') {
            showMenu();
        }

        recursiveAsyncReadLine();
    });
};

recursiveAsyncReadLine();

function showMenu() {
    console.log("\n---MENU---\n/remetente nome -- define um nome para remetente\n/destinatario nome -- define um destinatário para a transacão\n/qtd valor -- define o valor da transação\n/transacao -- Cria uma transação que será armazenada na blockChain\n/showBlock -- Mostra as informações armazenadas no blockChain\n/menu -- Para mostrar o menu da aplicação\n/exit-- Sai da aplicação\n");
}
