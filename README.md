# Sobre o projeto
Projeto de implementação de BlockChain em NodeJs para aprendizado de tecnologias P2P para a disciplina de Sistemas Distribuídos 2019-2.

# BlockChain
BlockChain é basicamente uma cadeia infinita de blocos que cresce rapidamente, onde cada bloco representa um dado imutável, o que o torna super poderoso em termos de lidar com dados sensíveis, são amplamente utilizados para criptomoedas (Bitcoin, Lithium ...).

Usa algoritmos de criptografia para impedir a extração de dados por hash de bloco, onde cada bloco tem seu próprio hash exclusivo na cadeia, o que permite que a cadeia esteja protegida contra modificações não autorizadas. (SHA256, codificação base64 ...)

![](https://www.portaldoblockchain.com.br/wp-content/uploads/2018/05/blockchain-3326155_1920-687x430.png)

### Primeiramente deve-se instalar uma ferramenta  para criação de hashs de objetos
    npm install object-hash --save

### blockChain.js
Esse arquivo contém todas as funcionalidades necessárias para o funcionamento do nosso projeto BlockChain.
O blockchain é composto por uma lista de blocos interligados e uma lista de transações entre esses blocos.
Principais Funções:
- adicionarBloco(hashAnterior): essa função adiciona um bloco à lista de blocos a partir do hashAnterior que é usado para manter a ligação entre os blocos que compõem o blockChain;
- adicionarNovaTransacao(remetente, destinatario, quantidade): essa função é responsável por criar uma transação (no caso dinheiro), "saindo" de um remetente, até um destinatário;

### main.js
As funções verificaProva = (prova) e proofOfWork = () estão relacionadas a um dos fundamentos do BlockChain, mining ou mineração. Basicamente o conceito de mineração em BlockChain é o áto de adicionar novos blocos que mantêm novas transações na cadeia grande de blocos, mas não é tão simples, cada moeda tem seus próprios métodos de validação que permitem adicionar blocos apenas ao finalizar o hash certo (e isso é mineração).

Esse arquivo também implementa um simple menu textual para interação com o BlockChain, com as seguintes funções:
- /remetente nome - Define um nome para remetente
- /destinatario nome - Define um destinatário para a transacão
- /qtd valor - Define o valor da transação
- /transacao - Realiza a transação com os valores de Remetente, Destinatário e valor
- /showBlock - Mostra ao usuário o conteúdo armazenado no BlockChain
- /exit - desliga a aplicação

### Inicializando
Para inicializar o programa deve-se dirigir pelo terminal até o local onde os arquivos foram clonados e digitar o código:

    node main.js
Exemplos de funcionamento:
![](https://i.ibb.co/xDbMgcg/Captura-de-tela-de-2019-12-15-18-30-29.png)

### Referências

https://medium.com/tableless/como-implementar-o-blockchain-em-javascript-com-menos-de-80-linhas-c2ff4852ee13

https://ipenywis.com/tutorials/Let's-Create-A-BlockChain-On-Node.js

