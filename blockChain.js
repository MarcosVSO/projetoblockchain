let hash = require('object-hash');

class BlockChain {

    constructor() {
        this.chain = [];
        this.transacoes_atuais = [];
    }

    // Adiciona bloco a corrente(chain)
    adicionarBloco(hashAnterior) {
        let bloco = {
            indice: this.chain.length + 1,
            timestamp: Date.now(),
            transacoes: this.transacoes_atuais,
            hashAnterior: hashAnterior,
        };
        // Cria o hash do bloco
        this.hash = hash(bloco);
        //Adiciona o bloco a corrente(chain)
        this.chain.push(bloco);
        this.transacoes_atuais = [];
        return bloco;
    }

    // Adiciona transação à lista de transacoes do bloco
    adicionarNovaTransacao(remetente, destinatario, quantidade) {
        this.transacoes_atuais.push({ remetente, destinatario, quantidade });
    }

    //Retorna último bloco adicionado à chain
    ultimoBloco() {
        return this.chain.slice(-1)[0];
    }
    //Verifica se a chain está vazia
    chainVazia() {
        return this.chain.length == 0;
    }
}
module.exports = BlockChain;